const config = require('./config');
const {query} = require ('./query');

const callback = (err, result) => {
    if (err) return cb(err);
    const coins = result[0].coins || false;
    if (coins >= config.withdraw.minEarnedToWithdraw)
        return cb(null, true);
    return cb(null, false);
}

module.exports = {
    earnedEnoughToWithdraw: async (gainid, cb) => {
        connection.query(query,[gainid, gainid, gainid], callback(cb));
    }
}