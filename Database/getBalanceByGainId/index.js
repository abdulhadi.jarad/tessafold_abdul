const callback = (err, result, cb) => {
    if (err) return cb(err);
    assert(result.length === 1);
    return cb(null, result[0].balance);
}

module.exports = {
    getBalanceByGainId: async (gainid, cb) => {
        connection.query(query, [gainid], (err, result) => callback(err, result, cb));
    },
}