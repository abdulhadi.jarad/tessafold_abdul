const callback = (err, result, cb) => {
    if (err) return cb(err);
    assert(result.length === 2);
    cb(null, result);
}
module.exports = {
    insertPendingSiteGiftCardWithdraw: async  (
        gainid,
        coinAmount,
        cardType,
        countryCode,
        date,
        warningMessage,
        cb
    ) => {
        connection.query(
            query,
            [gainid, date, warningMessage, coinAmount, cardType, date, countryCode],
            (err, result) => callback(err, result, cb)
        );
    },
}