const callback = (err, result, cb) => {
    if (err) return cb(err);
    assert(result.length === 2);
    cb(null, result);
}
module.exports = {
    insertSiteGiftCardWithdrawal: async (
        gainid,
        coinAmount,
        cardCode,
        cardType,
        countryCode,
        date,
        approver,
        cb
    ) => {
        connection.query(
            query,
            [gainid, date, approver, coinAmount, cardCode, cardType, date, countryCode],
            (err, result) => callback(err, result, cb)
        );
    }
}