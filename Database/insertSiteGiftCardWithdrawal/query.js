export const query = `
      INSERT INTO withdraw (gainid, date, approver) VALUES (?, ?, ?);
      INSERT INTO site_gift_card_withdraw (withdrawid, coinamount, card_code, card_type, date, country_code) VALUES
      (LAST_INSERT_ID(), ?, ?, ?, ?, ?)
    `;