const earnedEnoughToWithdraw = require('./earnedEnoughToWithdraw');
const getAccountStanding = require('./getAccountStanding');
const getBalanceByGainId = require('./getBalanceByGainId');
const insertPendingSiteGiftCardWithdraw = require('./insertPendingSiteGiftCardWithdraw');
const insertSiteGiftCardWithdrawal = require('./insertSiteGiftCardWithdrawal');
const isDefaultUserEmailVerified = require('./isDefaultUserEmailVerified');
const updateBalanceByGainId = require('./updateBalanceByGainId');

const db = {
    earnedEnoughToWithdraw,
    getAccountStanding,
    getBalanceByGainId,
    insertPendingSiteGiftCardWithdraw,
    insertSiteGiftCardWithdrawal,
    isDefaultUserEmailVerified,
    updateBalanceByGainId
}

module.exports = Promise.promisifyAll(db) || db;