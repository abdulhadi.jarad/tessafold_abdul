export const query = `
      SELECT * FROM banned WHERE gainid = ?;
      SELECT * FROM frozen WHERE gainid = ?;
      SELECT * FROM muted WHERE gainid = ?;
      SELECT * FROM countrybanned WHERE gainid = ?;
      SELECT deleted FROM users WHERE gainid = ?;
    `;