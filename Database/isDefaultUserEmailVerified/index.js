
const callback = (err, result) => {
    if (err) {
        return cb(err);
    }
    if (result && result[0] && result[0].email_confirmed) {
        return cb(null, true);
    } else if (!result[0]) {
        return cb(null, true);
    }
    return cb(null, false);
}

module.exports = {
    isDefaultUserEmailVerified: async (gainid, cb) => {
        connection.query(query, [gainid], callback(cb));
    }
}