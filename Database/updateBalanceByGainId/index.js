const { nestedQueries } = require("./query");

const _1thcallback = (err, result, cb) => {
        if (err) return cb(err);
        assert(result.affectedRows === 1);
        connection.query(
            nestedQueries,
            [gainid, amount, gainid],
            (err, result) => _2thcallback(err, result, cb)
        );
    }

const _2thcallback = (err, result, cb) => {
    if (err) return cb(err);
    assert(result.affectedRows === 1);
    cb(null);
}

module.exports = {
    updateBalanceByGainId: async (gainid, amount, cb) => {
        connection.query(
            query,
            [amount, gainid],
            (err, result) => _1thcallback(err, result, cb)
        );
    }
}