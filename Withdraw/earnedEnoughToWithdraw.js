const { Print } = require("../Helpers/Print");

module.exports = {
    earnedEnoughToWithdraw: db.earnedEnoughToWithdraw(socketuser.gainid, (err, earnedEnoughBool) => {
        
        if (errorValidator(err))
            feedback(Messages.serverError)

        if (!earnedEnoughBool) 
            return feedback(minEarnText);

        return GiftcardService.isGiftCardInStock(type, coinAmount, countryCode).then((result) => {
                // if it's on stock continue as usual
                if (result) return notVerified();
                // otherwise, tell the user to pick another card.
                return feedback(Messages.outOfStock);
            })
            .catch((err) => {
                // if there's an error, it can't be determined if the card is in stock or not
                Print(err)
                return notVerified();
            });
    })
}