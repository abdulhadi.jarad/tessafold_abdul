const { getGiftByGainId } = require("../Withdraw/getGiftcard");
const earnedEnoughToWithdraw = require("../Withdraw/earnedEnoughToWithdraw");
const getAccountStanding = require("../Withdraw/getAccountStanding");
const getBalanceByGainId = require("../Withdraw/getBalanceByGainId");
const { isDefaultUserEmailVerified } = require("../Withdraw/isDefaultUserEmailVerified");
const { insertSiteGiftCardWithdraw } = require("./insertSiteGiftCardWithdrawal ");
const { onsiteGiftcardWithdraw } = require("./onsiteGiftcardWithdraw");

class Withdraw {
    socket = null;
    socketUser = null;
    verified = false;

    // I didn't honstley understand how can i know if user is verified or not

    constructor(socket, socketUser) {
        this.socket = socket;
        this.socketUser = socketUser;
    }

    checkUserVerification = () => this.verified ? operateVerifiedUser() : operateNotVerifiedUser()

    operateNotVerifiedUser = () => {
        await onsiteGiftcardWithdraw(this.socketUser.gainid)
        await getAccountStanding(this.socketUser.gainid);
        await isDefaultUserEmailVerified(this.socketUser.gainid);
        await getBalanceByGainId(this.socketUser.gainid);
        await earnedEnoughToWithdraw(this.socketUser.gainid);
    }
    
    operateVerifiedUser = () => {
        await getGiftByGainId(this.socket.gainid);
        await insertSiteGiftCardWithdraw(this.socket.gainid)
    }
}

module.exports = Withdraw;