const { errorValidator } = require("../Helpers/errorValidator");
const { Print } = require("../Helpers/Print");

module.exports = {
    getGiftcard: db.getGiftcard(socketuser.gainid, (err, {  }) => {
        if (errorValidator(err)){
            Notifications.storeNotification( socketuser.gainid, 'Info', GiftCardUnavailableError, Messages.GiftCardUnavailableError );
            return feedback(Messages.GiftCardUnavailableError);
        }
    })
}