const { errorValidator } = require("../Helpers/errorValidator");
const { Print } = require("../Helpers/Print");

module.exports = {
    getAccountStanding: db.getAccountStanding(socketuser.gainid, (err, { frozen, banned }) => {
        if (errorValidator(err))
            return feedback(Messages.serverError)
        else if (frozen || banned)
            feedback(Messages.bannedCountry)
    })
}